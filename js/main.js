/**
 * IMAGE FILTERS
 */

"use strict";

/**
 * Function take 6 images from the img folder and display them inline
 */
var imagePath = 'img/image';
var imageFile = '.jpg';
for (var i = 1; i <= 6; i++) {
    document.getElementById('images-to-choose').innerHTML += (
        '<div class="gallery col-md-2">' +
        '<img id="' + i + '" src="' + imagePath + i + imageFile + '" height="100">' +
        '</div>'
    );
}

var imgGlobalId;

/**
 * Function handler for click element 
 */
function getEventTarget(e) {
    e = e || window.event;
    return e.target || e.srcElement;
}


/**
 * Function return src of the clicked image and assign to variable clickedImage
 */
var clickedImage = document.getElementById('images-to-choose');
clickedImage.onclick = function (event) {
    var target = getEventTarget(event);

    var chosenImagePath = target.src;
    imgGlobalId = target.id;
    document.getElementById('chosen-image').innerHTML = "";
    document.getElementById('chosen-image').innerHTML += ('<div>' + '<img id="chosen-image-img" class="chosen-image-img" src="' + chosenImagePath + '">' + '</div>');
};


/**
 * Filter global variables
 * Define which filter is going to be applied
 */
var grayScaleBtn = false;
var negationBtn = false;
var brightnessBtn = false;
var contrastBtn = false;
var thresholdBtn = false;

var stretchingBtn = false;
var equalizationBtn = false;

var averageBtn = false;
var gaussianBlurBtn = false;
var sharpeningBtn = false;
var crossBtn = false;
var irisBtn = false;



/**
 * Getting all buttons for filter functions
 */
var grayScaleFilterBtn = document.getElementById('grayscale');
var negationFilterBtn = document.getElementById('negation');
var brightnessFilterBtn = document.getElementById('brightness');
var contrastFilterBtn = document.getElementById('contrast');
var thresholdFilterBtn = document.getElementById('threshold');


/**
 * Histogram Stretching and Equalization
 */
var histogramStretchingBtn = document.getElementById("histogram-stretching");
var histogramEqualizationBtn = document.getElementById("histogram-equalization");
var redColorsTableMinValue;
var redColorsTableMinValue;
var greenColorsTableMinValue;
var greenColorsTableMinValue;
var blueColorsTableMinValue;
var blueColorsTableMinValue;

/**
 * Filters
 */
var averageFilterBtn = document.getElementById('average');
var GaussianBlurFilterBtn = document.getElementById('gaussian-blur');
var SharpeningFilterBtn = document.getElementById('sharpening');
var SoberFilterBtn = document.getElementById('sober');
var CrossFilterBtn = document.getElementById('cross');

var irisFilterBtn = document.getElementById('iris');

/**
 * Function responsible to get which button filter was clicked
 * and triggering filter function on image object
 */
grayScaleFilterBtn.onclick = function (event) {
    console.log('gray');
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = thresholdBtn = stretchingBtn = equalizationBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    grayScaleBtn = true;
    imgObj.src = applyFilter();
};
negationFilterBtn.onclick = function (event) {
    console.log('negation');
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = grayScaleBtn = thresholdBtn = stretchingBtn = equalizationBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    negationBtn = true;
    imgObj.src = applyFilter();
};
brightnessFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = negationBtn = grayScaleBtn = thresholdBtn = stretchingBtn = equalizationBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    brightnessBtn = true;
    imgObj.src = applyFilter();
};
contrastFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    brightnessBtn = negationBtn = grayScaleBtn = thresholdBtn = stretchingBtn = equalizationBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    contrastBtn = true;
    imgObj.src = applyFilter();
};
thresholdFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = grayScaleBtn = stretchingBtn = equalizationBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    thresholdBtn = true;
    imgObj.src = applyFilter();
};


histogramStretchingBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = thresholdBtn = grayScaleBtn = equalizationBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    stretchingBtn = true;
    imgObj.src = applyFilter();
};
histogramEqualizationBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = grayScaleBtn = thresholdBtn = grayScaleBtn = stretchingBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    equalizationBtn = true;
    imgObj.src = applyFilter();
};


averageFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = grayScaleBtn = stretchingBtn = equalizationBtn = thresholdBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    averageBtn = true;
    imgObj.src = applyFilter();
};
GaussianBlurFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = grayScaleBtn = stretchingBtn = equalizationBtn = thresholdBtn = averageBtn = sharpeningBtn = soberBtn = crossBtn = irisBtn = false;
    gaussianBlurBtn = true;
    imgObj.src = applyFilter();
};
SharpeningFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = grayScaleBtn = stretchingBtn = equalizationBtn = thresholdBtn = averageBtn = gaussianBlurBtn = soberBtn = crossBtn = irisBtn = false;
    sharpeningBtn = true;
    imgObj.src = applyFilter();
};
SoberFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = grayScaleBtn = stretchingBtn = equalizationBtn = thresholdBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = crossBtn = irisBtn = false;
    soberBtn = true;
    imgObj.src = applyFilter();
};
CrossFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = grayScaleBtn = stretchingBtn = equalizationBtn = thresholdBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = irisBtn = false;
    crossBtn = true;
    imgObj.src = applyFilter();
};

irisFilterBtn.onclick = function (event) {
    var imgObj = document.getElementById('chosen-image-img');
    imgObjGlobal = document.getElementById('chosen-image-img');
    var target = getEventTarget(event);
    contrastBtn = brightnessBtn = negationBtn = grayScaleBtn = stretchingBtn = equalizationBtn = thresholdBtn = averageBtn = gaussianBlurBtn = sharpeningBtn = soberBtn = crossBtn = false;
    irisBtn = true;
    imgObj.src = applyFilter();
};

/**
 * Slider for brightness, contrast, threshold
 */
var slider = document.getElementById("myRange");
var output = document.getElementById("valueOfSlider");
output.innerHTML = slider.value; // Display the default slider value

/**
 * Default values
 */
var brightnessLevel = 28;
var contrastLevel = 40;
var thresholdNumber = 78;


// Update the current slider value (each time you drag the slider handle)
slider.oninput = function () {
    output.innerHTML = this.value;
    brightnessLevel = contrastLevel = thresholdNumber = parseInt(this.value);
}

/**
 * Table of colors for histogram
 */
var redColorsTable = new Array(256);
var greenColorsTable = new Array(256);
var blueColorsTable = new Array(256);

var cropingCenter = new Array(2);
var cropingRadius = new Array(2);
var radius, irisLength;
var choosingRadius;

function getMousePos(canvas, evt) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: evt.clientX - rect.left,
      y: evt.clientY - rect.top
    };
}

document.getElementById("crop-image").addEventListener("click", startCroping);
function startCroping(event) {
    var canvas = document.getElementById('chosen-image-img');
    canvas.addEventListener("click", canvasClicked);
    
    function canvasClicked(event) {
        
          canvas.addEventListener('click', function(evt) {
            var mousePos = getMousePos(canvas, evt);
            console.log('Mouse position: ' + mousePos.x + ',' + mousePos.y);

            if(choosingRadius == true) {
                console.log('calculating croping radius');
                cropingRadius[0] = mousePos.x;
                cropingRadius[1] = mousePos.y;
                radius = Math.sqrt( (cropingRadius[0] - cropingCenter[0])^2 +
                    (cropingRadius[1] - cropingCenter[1])^2 );
                irisLength = radius * 2.5;
                console.log('radius: ' + radius);
                choosingRadius = false;

            }
            else {
                console.log('calculating croping center');
                cropingCenter[0] = mousePos.x;
                cropingCenter[1] = mousePos.y;
                choosingRadius = true;
            }
          }, false);  
    }
}


/**
 * Main function performing filter operations on imgObj
 * @param {*imgObj} imgObj is main image which will be applied to filters
 */
function applyFilter() {
    var imgObj = document.getElementById('chosen-image-img');
    var canvas = document.createElement('canvas');
    var canvasContext = canvas.getContext('2d');

    for (var i = 0; i < 256; i++) {
        redColorsTable[i] = greenColorsTable[i] = blueColorsTable[i] = 0;
    }

    var imgW = imgObj.width;
    var imgH = imgObj.height;
    canvas.width = imgW;
    canvas.height = imgH;


    canvasContext.drawImage(imgObj, 0, 0);
    var imgPixels = canvasContext.getImageData(0, 0, imgW, imgH);
    var imgPixelsCopy = canvasContext.getImageData(0, 0, imgW, imgH);

    var imgPixelsDefault = imgPixels;

    if (grayScaleBtn) {
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;
                var avg = (imgPixels.data[i] * 0.3 + imgPixels.data[i + 1] * 0.59 + imgPixels.data[i + 2] * 0.11) / 3;
                imgPixels.data[i] = avg;
                imgPixels.data[i + 1] = avg;
                imgPixels.data[i + 2] = avg;

                redColorsTable[avg]++;
                greenColorsTable[avg]++;
                blueColorsTable[avg]++;
            }
        }
    }
    else if (negationBtn) {
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;
                imgPixels.data[i] = 255 - imgPixels.data[i];
                imgPixels.data[i + 1] = 255 - imgPixels.data[i + 1];
                imgPixels.data[i + 2] = 255 - imgPixels.data[i + 2];

                redColorsTable[imgPixels.data[i]]++;
                greenColorsTable[imgPixels.data[i + 1]]++;
                blueColorsTable[imgPixels.data[i + 2]]++;
            }
        }
    }
    else if (brightnessBtn) {
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;
                // var avg = (imgPixels.data[i]*0.3 + imgPixels.data[i + 1]*0.59 + imgPixels.data[i + 2]*0.11) / 3;
                imgPixels.data[i] += brightnessLevel;
                imgPixels.data[i + 1] += brightnessLevel;
                imgPixels.data[i + 2] += brightnessLevel;

                redColorsTable[imgPixels.data[i]]++;
                greenColorsTable[imgPixels.data[i + 1]]++;
                blueColorsTable[imgPixels.data[i + 2]]++;
            }
        }
    }
    else if (contrastBtn) {
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;

                imgPixels.data[i] = ((((imgPixels.data[i] / 255.0) - 0.5) * contrastLevel) + 0.5) * 255.0;
                imgPixels.data[i + 1] = ((((imgPixels.data[i + 1] / 255.0) - 0.5) * contrastLevel) + 0.5) * 255.0;
                imgPixels.data[i + 2] = ((((imgPixels.data[i + 2] / 255.0) - 0.5) * contrastLevel) + 0.5) * 255.0;


                if (imgPixels.data[i] > 255) { imgPixels.data[i] = 255; }
                else if (imgPixels.data[i] < 0) { imgPixels.data[i] = 0; }

                if (imgPixels.data[i + 1] > 255) { imgPixels.data[i + 1] = 255; }
                else if (imgPixels.data[i + 1] < 0) { imgPixels.data[i + 1] = 0; }

                if (imgPixels.data[i + 2] > 255) { imgPixels.data[i + 2] = 255; }
                else if (imgPixels.data[i + 2] < 0) { imgPixels.data[i + 2] = 0; }

                redColorsTable[imgPixels.data[i]]++;
                greenColorsTable[imgPixels.data[i + 1]]++;
                blueColorsTable[imgPixels.data[i + 2]]++;

            }
        }
    }
    else if (thresholdBtn) {
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;
                var r = imgPixels.data[i];
                var g = imgPixels.data[i + 1];
                var b = imgPixels.data[i + 2];
                var v = (0.2126 * r + 0.7152 * g + 0.0722 * b >= thresholdNumber) ? 255 : 0;
                imgPixels.data[i] = imgPixels.data[i + 1] = imgPixels.data[i + 2] = v

                redColorsTable[v]++;
                greenColorsTable[v]++;
                blueColorsTable[v]++;
            }
        }
    }
    else if (stretchingBtn) {
        var dynamicRed = redColorsTableMaxValue - redColorsTableMinValue;
        var dynamicGreen = greenColorsTableMaxValue - greenColorsTableMinValue;
        var dynamicBlue = blueColorsTableMaxValue - blueColorsTableMinValue;

        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {
                var i = (y * 4) * imgPixels.width + x * 4;
                imgPixels.data[i] = ((imgPixels.data[i] - redColorsTableMinValue) / dynamicRed) * 255;
                imgPixels.data[i + 1] = ((imgPixels.data[i + 1] - greenColorsTableMinValue) / dynamicGreen) * 255;
                imgPixels.data[i + 2] = ((imgPixels.data[i + 2] - blueColorsTableMinValue) / dynamicBlue) * 255;


                if (imgPixels.data[i] > 255) { imgPixels.data[i] = 255; }
                else if (imgPixels.data[i] < 0) { imgPixels.data[i] = 0; }

                if (imgPixels.data[i + 1] > 255) { imgPixels.data[i + 1] = 255; }
                else if (imgPixels.data[i + 1] < 0) { imgPixels.data[i + 1] = 0; }

                if (imgPixels.data[i + 2] > 255) { imgPixels.data[i + 2] = 255; }
                else if (imgPixels.data[i + 2] < 0) { imgPixels.data[i + 2] = 0; }

                redColorsTable[imgPixels.data[i]]++;
                greenColorsTable[imgPixels.data[i + 1]]++;
                blueColorsTable[imgPixels.data[i + 2]]++;
            }
        }
    }
    else if (averageBtn) {
        var matrixSize = 3;
        var filterOffset = (matrixSize - 1) / 2;
        var i = 0; // byteOffset
        var calcOffset = 0;
        var red = 0;
        var green = 0;
        var blue = 0;

        for (var y = filterOffset; y < imgPixels.height - filterOffset; y++) {
            for (var x = filterOffset; x < imgPixels.width - filterOffset; x++) {
                i = (y * 4) * imgPixels.width + x * 4;

                blue = 0;
                green = 0;
                red = 0;

                for (var filterY = -filterOffset; filterY <= filterOffset; filterY++) {
                    for (var filterX = -filterOffset; filterX <= filterOffset; filterX++) {

                        calcOffset = i + (filterX * 4) + (filterY * imgPixels.width * 4);

                        red += imgPixels.data[i];
                        green += imgPixels.data[i + 1];
                        blue += imgPixels.data[i + 2];
                    }
                }

                red = red / matrixSize;
                green = green / matrixSize;
                blue = blue / matrixSize;

                imgPixels.data[i] = red;
                imgPixels.data[i + 1] = green;
                imgPixels.data[i + 2] = blue;

                redColorsTable[imgPixels.data[i]]++;
                greenColorsTable[imgPixels.data[i + 1]]++;
                blueColorsTable[imgPixels.data[i + 2]]++;
            }
        }
    }
    else if (sharpeningBtn || gaussianBlurBtn) {
        if (gaussianBlurBtn) {
            var weights =
                [1 / 9, 1 / 9, 1 / 9,
                1 / 9, 1 / 9, 1 / 9,
                1 / 9, 1 / 9, 1 / 9];
        }
        else if (sharpeningBtn) {
            var weights =
                [0, -1, 0,
                    -1, 5, -1,
                    0, -1, 0];
        }

        var side = Math.round(Math.sqrt(weights.length)); // how many elements in a row or column
        var halfSide = Math.floor(side / 2);
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {
                var sy = y;
                var sx = x;
                var dstOff = (y * imgPixels.width + x) * 4;
                // calculate the weighed sum of the source image pixels that
                // fall under the convolution matrix
                var red = 0, green = 0, blue = 0;
                for (var cy = 0; cy < side; cy++) {
                    for (var cx = 0; cx < side; cx++) {

                        var scy = sy + cy - halfSide;
                        var scx = sx + cx - halfSide;

                        if (scy >= 0 && scy < imgPixels.height && scx >= 0 && scx < imgPixels.width) {
                            var srcOff = (scy * imgPixels.width + scx) * 4;
                            var wt = weights[cy * side + cx];
                            red += imgPixels.data[srcOff] * wt;
                            green += imgPixels.data[srcOff + 1] * wt;
                            blue += imgPixels.data[srcOff + 2] * wt;
                        }
                    }
                }
                imgPixels.data[dstOff] = red;
                imgPixels.data[dstOff + 1] = green;
                imgPixels.data[dstOff + 2] = blue;

                redColorsTable[imgPixels.data[dstOff]]++;
                greenColorsTable[imgPixels.data[dstOff + 1]]++;
                blueColorsTable[imgPixels.data[dstOff + 2]]++;
            }
        }
    }
    else if (soberBtn) {

        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;
                var avg = (imgPixels.data[i] * 0.3 + imgPixels.data[i + 1] * 0.59 + imgPixels.data[i + 2] * 0.11) / 3;
                imgPixels.data[i] = avg;
                imgPixels.data[i + 1] = avg;
                imgPixels.data[i + 2] = avg;

                redColorsTable[avg]++;
                greenColorsTable[avg]++;
                blueColorsTable[avg]++;
            }
        }


        function convoluteFloat32(pixels, weights) {
            var side = Math.round(Math.sqrt(weights.length));
            var halfSide = Math.floor(side/2);
  
            var src = pixels.data;
            var sw = pixels.width;
            var sh = pixels.height;
  
            var w = sw;
            var h = sh;
            var output = {
              width: w, height: h, data: new Float32Array(w*h*4)
            };
            var dst = output.data;
  
  
            for (var y=0; y<h; y++) {
              for (var x=0; x<w; x++) {
                var sy = y;
                var sx = x;
                var dstOff = (y*w+x)*4;
                var r=0, g=0, b=0, a=0;
                for (var cy=0; cy<side; cy++) {
                  for (var cx=0; cx<side; cx++) {
                    var scy = Math.min(sh-1, Math.max(0, sy + cy - halfSide));
                    var scx = Math.min(sw-1, Math.max(0, sx + cx - halfSide));
                    var srcOff = (scy*sw+scx)*4;
                    var wt = weights[cy*side+cx];
                    r += src[srcOff] * wt;
                    g += src[srcOff+1] * wt;
                    b += src[srcOff+2] * wt;
                  }
                }
                dst[dstOff] = r;
                dst[dstOff+1] = g;
                dst[dstOff+2] = b;
              }
            }
            return output;
        };

        var vertical = convoluteFloat32(imgPixels,
            [-1, -2, -1,
            0, 0, 0,
            1, 2, 1]);

        var horizontal = convoluteFloat32(imgPixels,
            [-1, 0, 1,
            -2, 0, 2,
            -1, 0, 1]);

        for (var i = 0; i < imgPixels.data.length; i += 4) {
            var v = Math.abs(vertical.data[i]);
            imgPixels.data[i] = v;
            var h = Math.abs(horizontal.data[i]);
            imgPixels.data[i + 1] = h;
            imgPixels.data[i + 2] = (v + h) / 4;
            imgPixels.data[i + 3] = 255;
        }

    }
    else if (crossBtn) {
        roberts_cross_v = [
            [0, 0, 0],
            [0, 1, 0],
            [0, 0, -1]];

        roberts_cross_h = [
            [0, 0, 0],
            [0, 0, 1],
            [0, -1, 0]];
    }
    else if(irisBtn)  {
        // contrast
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;
                imgPixels.data[i] = 255 - imgPixels.data[i];
                imgPixels.data[i + 1] = 255 - imgPixels.data[i + 1];
                imgPixels.data[i + 2] = 255 - imgPixels.data[i + 2];

                redColorsTable[imgPixels.data[i]]++;
                greenColorsTable[imgPixels.data[i + 1]]++;
                blueColorsTable[imgPixels.data[i + 2]]++;
            }
        }
        // grayscale
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;
                var avg = (imgPixels.data[i] * 0.3 + imgPixels.data[i + 1] * 0.59 + imgPixels.data[i + 2] * 0.11) / 3;
                imgPixels.data[i] = avg;
                imgPixels.data[i + 1] = avg;
                imgPixels.data[i + 2] = avg;

                redColorsTable[avg]++;
                greenColorsTable[avg]++;
                blueColorsTable[avg]++;
            }
        }
        // threshold
        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {

                var i = (y * 4) * imgPixels.width + x * 4;
                var r = imgPixels.data[i];
                var g = imgPixels.data[i + 1];
                var b = imgPixels.data[i + 2];
                var v = (0.2126 * r + 0.7152 * g + 0.0722 * b >= thresholdNumber) ? 255 : 0;
                imgPixels.data[i] = imgPixels.data[i + 1] = imgPixels.data[i + 2] = v

                redColorsTable[v]++;
                greenColorsTable[v]++;
                blueColorsTable[v]++;
            }
        }

        var binaryImageArr = zeros([imgPixels.width, imgPixels.height]);

        for (var y = 0; y < imgPixels.height; y++) {
            for (var x = 0; x < imgPixels.width; x++) {
                var i = (y * 4) * imgPixels.width + x * 4;
                
                // if pixel is NOT Black = is White
                if( imgPixels.data[i] != 0 && 
                    imgPixels.data[i + 1] != 0 &&
                    imgPixels.data[i + 2] != 0 ) {

                        binaryImageArr[x][y] = 1;
                        imgPixels.data[i] = 255;
                        imgPixels.data[i + 1] = 255;
                        imgPixels.data[i + 2] = 255;

                } else { // is Black
                    binaryImageArr[x][y] = 0;
                    imgPixels.data[i] = 0;
                    imgPixels.data[i + 1] = 0;
                    imgPixels.data[i + 2] = 0;
                }

            }
        }
 
        var maximalRectangle = MaximalRectangle(binaryImageArr);
        cw = imgPixels.width;
        ch = imgPixels.height;
        canvasContext.drawImage(imgObj, 0, 0);
        canvasContext.globalCompositeOperation = 'destination-in';
        canvasContext.beginPath();

        switch(imgGlobalId) {
            case "1": 
                pixelHeightValue = 11;
                break;
            case "2": 
                pixelHeightValue = 2;
                break;
            case "3": 
                pixelHeightValue = 8;
                break;
            case "4":
                pixelHeightValue = 1.5;
                break;
            case "5": 
                pixelHeightValue = 3.8;
                break;
            case "6": 
                pixelHeightValue = 1.8;
                break;
            default:
                pixelHeightValue = 2;
        }
        console.log(pixelHeightValue + ' - '+ imgGlobalId);

        canvasContext.arc(maximalRectangle.centerRow, maximalRectangle.centerCol, maximalRectangle.radius * pixelHeightValue, 0, Math.PI * 2);        

        canvasContext.closePath();
        canvasContext.fill();

        
        // canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
        return canvas.toDataURL();

    }

    function zeros(dimensions) {
        var array = [];
    
        for (var i = 0; i < dimensions[0]; ++i) {
            array.push(dimensions.length == 1 ? 0 : zeros(dimensions.slice(1)));
        }
    
        return array;
    }
    
    /* Maximal Rectangle: (0,0) => upper-left corner */
    function MaximalRectangle(matrix) {
        var m; // iterator for columns
        var n; // iterator for rows
        var M = matrix[0].length; // number of columns;
        var N = matrix.length; // number of rows
        var c = []; // linear cache
        var s = []; // stack of {col, row} pairs
        var best_ll = {col: 0, row: 0}; // lower-left corner
        var best_ur = {col: -1, row: -1}; // upper-right corner
        var best_area = 0; // int. Superfluous, since you can compute it from `best_ll` and `best_ur`
        
        for (m = 0; m != M + 1; ++m) {
            c[m] = 0;
            s[m] = {col: 0, row: 0};
        }
        for (n = 0; n != N; ++n) {
            for (m = 0; m != M; ++m) {
                c[m] = matrix[n][m] ? (c[m] + 1) : 0; // update cache
            }
            var open_width = 0;
            for (m = 0; m != M + 1; ++m) {
                if (c[m] > open_width) { /* Open new rectangle? */
                    s.push({col: m, row: open_width});
                    open_width = c[m];
                } else if (c[m] < open_width) { /* Close rectangle(s)? */
                    var m0;
                    var n0;
                    var area;
                    do {
                        var cell = s.pop();
                        m0 = cell.col;
                        n0 = cell.row;
                        area = open_width * (m - m0);
                        if (area > best_area) {
                            best_area = area;
                            best_ll = {col: m0, row: n};
                            best_ur = {col: m - 1, row: n - open_width + 1};
                        }
                        open_width = n0;
                    } while (c[m] < open_width);
                    open_width = c[m];
                    if (open_width != 0) {
                        s.push({col: m0, row: n0});
                    }
                }
            }
        }
        return {
            best_area: best_area,    // size of all pixels of the biggest object
            ll_col: best_ll.col + 1, // left column
            ll_row: best_ll.row + 1, // bottom row
            ur_col: best_ur.col + 1, // right column
            ur_row: best_ur.row + 1, // top row
            radius_small: (best_ur.col - best_ll.col) / 2, // radius of the black iris
            radius: (best_ur.col - best_ll.col) / 2 * 2.5, // radius of the eye
            centerCol: best_ll.col + ((best_ur.col - best_ll.col) / 2), //  center X point
            centerRow: best_ll.row + ((best_ur.row - best_ll.row) / 2)  //  center Y point
        }
    }


    canvasContext.putImageData(imgPixels, 0, 0, 0, 0, imgPixels.width, imgPixels.height);
    return canvas.toDataURL();
};








/**
 * BAR GRAPH
 */

/**
 * Bar Graph Constructor
 */
function BarGraph(ctx) {

    // Private properties and methods
    var that = this;
    var startArr;
    var endArr;

    // Draw method updates the canvas with the current display
    var draw = function (arr) {

        var numOfBars = arr.length;
        var barWidth;
        var barHeight;
        var border = 2;
        var ratio;
        var maxBarHeight;
        var gradient;
        var largestValue;
        var graphAreaX = 0;
        var graphAreaY = 0;
        var graphAreaWidth = that.width;
        var graphAreaHeight = that.height;
        var i;

        // Update the dimensions of the canvas only if they have changed
        if (ctx.canvas.width !== that.width || ctx.canvas.height !== that.height) {
            ctx.canvas.width = that.width;
            ctx.canvas.height = that.height;
        }

        // Draw the background color
        ctx.fillStyle = that.backgroundColor;
        ctx.fillRect(0, 0, that.width, that.height);


        // Calculate dimensions of the bar
        barWidth = graphAreaWidth / numOfBars - that.margin * 2;
        maxBarHeight = graphAreaHeight - 25;

        // Determine the largest value in the bar array
        var largestValue = 0;
        for (i = 0; i < arr.length; i += 1) {
            if (arr[i] > largestValue) {
                largestValue = arr[i];
            }
        }

        // For each bar
        for (i = 0; i < arr.length; i += 1) {
            // Set the ratio of current bar compared to the maximum
            if (that.maxValue) {
                ratio = arr[i] / that.maxValue;
            } else {
                ratio = arr[i] / largestValue;
            }

            barHeight = ratio * maxBarHeight;

            // Turn on shadow
            ctx.shadowOffsetX = 2;
            ctx.shadowOffsetY = 2;
            ctx.shadowBlur = 2;
            ctx.shadowColor = "#999";

            // Draw bar background
            ctx.fillStyle = "#333";
            ctx.fillRect(that.margin + i * that.width / numOfBars,
                graphAreaHeight - barHeight,
                barWidth,
                barHeight);

            // Turn off shadow
            ctx.shadowOffsetX = 0;
            ctx.shadowOffsetY = 0;
            ctx.shadowBlur = 0;

            // Draw bar color if it is large enough to be visible
            if (barHeight > border * 2) {
                // Create gradient
                gradient = ctx.createLinearGradient(0, 0, 0, graphAreaHeight);
                gradient.addColorStop(1 - ratio, that.colors[i % that.colors.length]);
                gradient.addColorStop(1, "#ffffff");

                ctx.fillStyle = gradient;
                // Fill rectangle with gradient
                ctx.fillRect(that.margin + i * that.width / numOfBars + border,
                    graphAreaHeight - barHeight + border,
                    barWidth - border * 2,
                    barHeight - border * 2);
            }


        }
    };

    // Public properties and methods

    this.width = 1070;
    this.height = 500;
    this.maxValue;
    this.margin = 1;
    this.colors = ["purple", "red", "green", "yellow"];
    this.curArr = [];
    this.backgroundColor = "#fff";
    this.animationInterval = 100;
    this.animationSteps = 10;

    // Update method sets the end bar array and starts the animation
    this.update = function (newArr) {
        that.curArr = newArr;
        draw(newArr);
    };
}

/**
 * Function returns index of biggest value in array arr
 */
function indexOfMax(arr) {
    if (arr.length === 0) {
        return -1;
    }

    var max = arr[0];
    var maxIndex = 0;

    for (var i = 1; i < arr.length; i++) {
        if (arr[i] > max) {
            maxIndex = i;
            max = arr[i];
        }
    }

    return maxIndex;
}
function indexOfMin(arr, min) {
    if (arr.length === 0) {
        return -1;
    }

    var minIndex = 0;

    for (var i = 0; i < arr.length; i++) {
        if (arr[i] < min && arr[i] != 0) {
            minIndex = i;
            min = arr[i];
        }
    }

    return minIndex;
}



/**
 * Function return src of the clicked image and assign to variable clickedImage
 */
var histogramBtn = document.getElementById('histogram');
histogramBtn.onclick = function (event) {

    function createCanvas(divName) {

        var div = document.getElementById(divName);
        div.innerHTML = "";
        var canvas = document.createElement('canvas');
        div.appendChild(canvas);
        if (typeof G_vmlCanvasManager != 'undefined') {
            canvas = G_vmlCanvasManager.initElement(canvas);
        }
        var ctx = canvas.getContext("2d");
        return ctx;
    }

    var rctx = createCanvas("redBarGraph");
    var gctx = createCanvas("greenBarGraph");
    var bctx = createCanvas("blueBarGraph");

    var redGraph = new BarGraph(rctx);
    redGraph.colors = ["#ff0000", "#ff9999", "#b20000", "#ffcccc"];
    redGraph.update(redColorsTable);

    var greenGraph = new BarGraph(gctx);
    greenGraph.colors = ["#00ff00", "#007f00", "#7fff7f", "#003300"];
    greenGraph.update(greenColorsTable);

    var blueGraph = new BarGraph(bctx);
    blueGraph.colors = ["#0000ff", "#6666ff", "#00007f", "#b2b2ff"];
    blueGraph.update(blueColorsTable);

    redColorsTableMaxValue = indexOfMax(redColorsTable);
    redColorsTableMinValue = indexOfMin(redColorsTable, redColorsTableMaxValue);

    greenColorsTableMaxValue = indexOfMax(greenColorsTable);
    greenColorsTableMinValue = indexOfMin(greenColorsTable, greenColorsTableMaxValue);

    blueColorsTableMaxValue = indexOfMax(blueColorsTable);
    blueColorsTableMinValue = indexOfMin(blueColorsTable, blueColorsTableMaxValue);

    redColorsTable = new Array(256);
    greenColorsTable = new Array(256);
    blueColorsTable = new Array(256);
};


/**
 * Save image to desktop
 */
function download() {
    var dt = globalCanvas.toDataURL();
    this.href = dt;
}
document.getElementById('downloadLnk').addEventListener('click', download, false);


//http://www.codinghands.co.uk/blog/2013/02/javascript-implementation-omn-maximal-rectangle-algorithm/ 
// https://www.codeproject.com/Articles/137623/Pupil-or-eyeball-detection-and-extraction-from-eye