## Biometry project for university course
 Lab1: Linear and non-linear pixel operations. Conversion to grayscale, negation, 
 brightness editing, contrast editing, thresholding, histogram, projection.
 Lab2: A program which finds an iris in an eye image.

## Project Author:
DonTom - Tomasz Marcińczyk
http://dontom.pl/

## Running project:
1. Install http-server globally: npm install http-server -g (https://www.npmjs.com/package/http-server)
2. Start the npm server : http-server
3. In main folder run 'grunt watch'
4. Open created server ex: http://127.0.0.1:8080

## Running application:
http://dontom.pl/biometry_1/


## Bitbucket application:
https://bitbucket.org/dontom_dev/biometry_filters 
